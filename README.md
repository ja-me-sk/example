[![pipeline status](https://gitlab.com/ja-me-sk/example/badges/master/pipeline.svg)](https://gitlab.com/ja-me-sk/example/commits/master)

This project is created from a GitLab [Project Template](https://docs.gitlab.com/ce/gitlab-basics/create-project.html)

Additions and changes to the project can be proposed on the [original project](https://gitlab.com/gitlab-org/project-templates/rails)
